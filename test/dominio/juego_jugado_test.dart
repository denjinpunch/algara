import 'package:tallerex1/caracteristicas/dominio/juego_jugado.dart';
import 'package:tallerex1/caracteristicas/dominio/problemas.dart';
import 'package:test/test.dart';

void main(){
test('diferente id y mismo nombre son diferentes', (){
 JuegoJugado j1 
 = JuegoJugado.constructor(nombrePropuesto: 'uno', idPropuesto: '1');
 JuegoJugado j2 
 = JuegoJugado.constructor(nombrePropuesto: 'uno', idPropuesto: '2');
 expect(j1 == j2, equals(false));
});
  test('mismo id y mismo nombre son iguales', () {
    JuegoJugado j1 
    = JuegoJugado.constructor(nombrePropuesto: 'uno', idPropuesto: '1');
    JuegoJugado j2 
    = JuegoJugado.constructor(nombrePropuesto: 'uno', idPropuesto: '1');
    expect(j1 == j2, equals(true));
  });
test('no se permite nombre vacio', (){
  expect(() => JuegoJugado.constructor(nombrePropuesto:'', idPropuesto: '1'),
  throwsA(isA<JuegoJugadoMalFormado>()));
});
test('no se permite id vacio', (){
  expect(() => JuegoJugado.constructor(nombrePropuesto:'j1', idPropuesto: ''),
  throwsA(isA<JuegoJugadoMalFormado>()));
});
}