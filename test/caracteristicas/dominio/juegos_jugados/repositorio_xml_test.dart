import 'package:tallerex1/caracteristicas/dominio/nick_formado.dart';
import 'package:tallerex1/caracteristicas/juegos_jugados/repositorio_xml.dart';
import 'package:test/test.dart';
import 'package:tallerex1/caracteristicas/juegos_jugados/repositorio_obtener_imagen.dart';

void main() {
  test('si le paso Benthor me regresa un solo XML', () async {
    RepositorioXml repositorio = RepositorioXmlPruebas();
    final resultado =
        await repositorio.obtenerXml(NickFormado.constructor('Benthor'));
    resultado.match((l) {
      expect(true, equals(false));
    }, (r) {
      expect(r.length, equals(1));
    });
  });
  test('si le paso Fokuleh me regresa 4 XML', () async {
    RepositorioXml repositorio = RepositorioXmlPruebas();
    final resultado =
        await repositorio.obtenerXml(NickFormado.constructor('Fokuleh'));
    resultado.match((l) {
      expect(true, equals(false));
    }, (r) {
      expect(r.length, equals(4));
    });
  });
  test('Probar Benthor online', () async {
    OnlineXml repositorio = OnlineXml();
    final resultado =
        await repositorio.obtenerXml(NickFormado.constructor('Benthor'));
    resultado.match((l) {
      expect(true, equals(false));
    }, (r) => expect(r.length, equals(15)));
  });

  test('Probar Fokuleh online', () async {
    OnlineXml repositorio = OnlineXml();
    final resultado =
        await repositorio.obtenerXml(NickFormado.constructor('Fokuleh'));
    resultado.match((l) {
      expect(true, equals(false));
    }, (r) => expect(r.length, equals(18)));
  });

}
