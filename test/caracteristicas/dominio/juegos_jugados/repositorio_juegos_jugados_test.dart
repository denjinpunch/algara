@Timeout(Duration(seconds: 100000))
import 'dart:io';
import 'package:tallerex1/caracteristicas/dominio/juego_jugado.dart';
import 'package:tallerex1/caracteristicas/dominio/nick_formado.dart';
import 'package:tallerex1/caracteristicas/juegos_jugados/repositorio_juegos_jugados.dart';
import 'package:tallerex1/caracteristicas/juegos_jugados/repositorio_obtener_imagen.dart';
import 'package:tallerex1/caracteristicas/juegos_jugados/repositorio_xml.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  test('Benthor jugo 2 juegos', () async {
    RepositorioXmlPruebas repositorioPruebas = RepositorioXmlPruebas();
    RepositorioJuegosJugadosPruebas repositorio =
        RepositorioJuegosJugadosPruebas(repositorioPruebas);
    final resultado = await repositorio
        .obtenerJuegosJugados(NickFormado.constructor('Benthor'));
    resultado.match((l) {
      expect(true, equals(false));
    }, (r) {
      expect(r.length, equals(2));
    });
  });
  // prueba de que takenoko esta entre los jugados
  test('takenoko es de los jugados por Benthor', () async {
    RepositorioXmlPruebas repositorioPruebas = RepositorioXmlPruebas();
    RepositorioJuegosJugadosPruebas repositorio =
        RepositorioJuegosJugadosPruebas(repositorioPruebas);
    final resultado = await repositorio
        .obtenerJuegosJugados(NickFormado.constructor('Benthor'));
    final takenoko = JuegoJugado.constructor(
        nombrePropuesto: 'Takenoko', idPropuesto: '70919');
    resultado.match((l) {
      expect(true, equals(false));
    }, (r) {
      expect(r.contains(takenoko), equals(true));
    });
  });
  // prueba de que el monopoly no esta entre los jugados
  test('Monopoly no es de los jugados por Benthor', () async {
    RepositorioXmlPruebas repositorioPruebas = RepositorioXmlPruebas();
    RepositorioJuegosJugadosPruebas repositorio =
        RepositorioJuegosJugadosPruebas(repositorioPruebas);
    final resultado = await repositorio
        .obtenerJuegosJugados(NickFormado.constructor('Benthor'));
    final monopoly =
        JuegoJugado.constructor(nombrePropuesto: 'Monopoly', idPropuesto: '7');
    resultado.match((l) {
      assert(false);
    }, (r) {
      expect(!r.contains(monopoly), equals(true));
    });
  });

  // prueba de que me regresa un problema si busco alguien que no es benthor
/*  test('Fokuleh debe de tener 4 paginas', () async {
    RepositorioJuegosJugadosPruebas repositorio =
        RepositorioJuegosJugadosPruebas();
    final resultado = await RepositorioJuegosJugadosPruebas();
  });  */
  //prueba que me regrese 6 juegos jugados fokuleh
  test('Fokuleh me debe regresar 6 juegos jugados',() async {
    RepositorioXmlPruebas repositorioPruebas = RepositorioXmlPruebas();
    RepositorioJuegosJugadosPruebas repositorio = RepositorioJuegosJugadosPruebas(repositorioPruebas);
    final resultado = await repositorio.obtenerJuegosJugados(NickFormado.constructor('Fokuleh'));
    resultado.match((l) {
      expect(true, equals(false));
    }, (r) {
      expect(r.length, equals(6));
    });
  });
    test('Benthor me debe regresar 2 juegos jugados', () async {
    RepositorioXmlPruebas repositorioPruebas = RepositorioXmlPruebas();
    RepositorioJuegosJugadosPruebas repositorio = RepositorioJuegosJugadosPruebas(repositorioPruebas);
    final resultado = await repositorio.obtenerJuegosJugados(NickFormado.constructor('Benthor'));
    resultado.match((l) {
      expect(true, equals(false));
    }, (r) {
      expect(r.length, equals(2));
    });
  });
    test('hacer txt fokuleh',() async {
    OnlineXml repositorioPruebas = OnlineXml();
    RepositorioJuegosJugadosPruebas repositorio = RepositorioJuegosJugadosPruebas(repositorioPruebas);
    const name = "fokuleh";
    final resultado = await repositorio.obtenerJuegosJugados(NickFormado.constructor(name));
    resultado.match((l) {
      expect(true, equals(false));
    }, (r) {
      String direction = "./test/caracteristicas/datos_txt/$name.txt";
      String data = "";
      for (JuegoJugado juegos in r){
      data = "$data${juegos.nombre} - ${juegos.id}\n";
      }
      var archive = File(direction);
      var write = archive.openWrite();
      write.write(data);
    });
  });
      test('hacer txt benthor',() async {
    OnlineXml repositorioPruebas = OnlineXml();
    RepositorioJuegosJugadosPruebas repositorio = RepositorioJuegosJugadosPruebas(repositorioPruebas);
    const name = "benthor";
    final resultado = await repositorio.obtenerJuegosJugados(NickFormado.constructor(name));
    resultado.match((l) {
      expect(true, equals(false));
    }, (r) {
      String direction = "./test/caracteristicas/datos_txt/$name.txt";
      String data = "";
      for (JuegoJugado juegos in r){
      data = "$data${juegos.nombre} - ${juegos.id}\n";
      }
      var archive = File(direction);
      var write = archive.openWrite();
      write.write(data);
    });
  });
       test('hacer txt agregado benthor', () async {
      OnlineXml repositorioPruebas = OnlineXml();
      RepositorioJuegosJugadosPruebas repositorio = RepositorioJuegosJugadosPruebas(repositorioPruebas);
      OnlineXmlImg thmb = OnlineXmlImg();
      const name='benthor';
      final resultado = await repositorio.obtenerJuegosJugados(NickFormado.constructor(name));
      String direction ="./test/caracteristicas/datos_txt/$name.txt";
      String data = "";
      resultado.match((l) {}, (r){
        for (JuegoJugado juegos in r) {
          data= "$data${juegos.id} - ${juegos.nombre}\n";
        }
      });
  List<String> content=[];
      for (var i = 0; i < data.split('\n').length; i++) {
        List<String>brap = data.split('\n')[i].split('-');
        var resultado=await thmb.obtenerThmb(brap[0]);
        resultado.match((l) => null, (r){
          content.add('${brap[0]} - ${brap[1]} - ${r[0]} - ${r[1]}\n');
          sleep(const Duration(milliseconds: 2000));
        }); 
      }
      var archive=File(direction);
      var write=archive.openWrite();
      write.write(content);
    });
       test('hacer txt agregado fokuleh', () async {
      OnlineXml repositorioPruebas = OnlineXml();
      RepositorioJuegosJugadosPruebas repositorio = RepositorioJuegosJugadosPruebas(repositorioPruebas);
      OnlineXmlImg thmb = OnlineXmlImg();
      const name='fokuleh';
      final resultado = await repositorio.obtenerJuegosJugados(NickFormado.constructor(name));
      String direction ="./test/caracteristicas/datos_txt/$name.txt";
      String data = "";
      resultado.match((l) {}, (r){
        for (JuegoJugado juegos in r) {
          data= "$data${juegos.id} - ${juegos.nombre}\n";
        }
      });
  List<String> content=[];
      for (var i = 0; i < data.split('\n').length; i++) {
        List<String>brap = data.split('\n')[i].split('-');
        var resultado=await thmb.obtenerThmb(brap[0]);
        resultado.match((l) => null, (r){
          content.add('${brap[0]} - ${brap[1]} - ${r[0]} - ${r[1]}\n');
          sleep(const Duration(milliseconds: 2000));
        }); 
      }
      var archive=File(direction);
      var write=archive.openWrite();
      write.write(content);
    });
}
