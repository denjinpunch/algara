import 'dart:io';

import 'package:tallerex1/caracteristicas/dominio/juego_jugado.dart';
import 'package:tallerex1/caracteristicas/dominio/nick_formado.dart';
import 'package:tallerex1/caracteristicas/juegos_jugados/repositorio_obtener_imagen.dart';
import 'package:tallerex1/caracteristicas/juegos_jugados/repositorio_juegos_jugados.dart';
import 'package:tallerex1/caracteristicas/juegos_jugados/repositorio_xml.dart';
import 'package:flutter_test/flutter_test.dart';
void main() {
  test('Probar thmb online', () async {
    OnlineXmlImg repositorio = OnlineXmlImg();
    final resultado = await repositorio.obtenerThmb('27760');
    resultado.match((l) {
      expect(true, equals(false));
    }, (r) => expect(r.length, equals(2)));
  });
    test('Probar thmb offline 1', () async {
    OfflineXmlImg repositorio=OfflineXmlImg();
    var resultado=await repositorio.obtenerThmb('27760');
    resultado.match((l) => null, (r) {
      expect(r[0], 'https://cf.geekdo-images.com/HQBb1F08jMdEN_G1us_M-A__thumb/img/eOkj9TpPl5e6mC4KlifrOquGN6A=/fit-in/200x150/filters:strip_icc()/pic2420317.jpg');
    });
  });
  test('Probar thmb offline 2', () async {
    OfflineXmlImg repositorio=OfflineXmlImg();
    var resultado=await repositorio.obtenerThmb('27760');
    resultado.match((l) => null, (r) {
      expect(r[1], 'Klaus Teuber');
    });
  });
}