import 'package:bloc_test/bloc_test.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:tallerex1/caracteristicas/bloc.dart';
import 'package:tallerex1/caracteristicas/dominio/nick_formado.dart';
import 'package:tallerex1/caracteristicas/repositorio_verificacion.dart';

void main() {
  blocTest<BlocVerificacion, EstadoVerificacion>(
    'debe tener inicialmente solicitando nombre',
    build: () => BlocVerificacion(),
    act: (bloc) => bloc.add(Creado()),
    expect: () => [isA<SolicitandoNombre>()],
  );

  blocTest<BlocVerificacion, EstadoVerificacion>(
    'cuando nombreRecibido es Benthor debo tener MostrandoNombreConfirmado',
    build: () => BlocVerificacion(),
    seed: () => SolicitandoNombre(),
    act: (bloc) => bloc.add(NombreRecibido(NickFormado.constructor('benthor'))),
    expect: () =>
        [isA<EsperandoConfirmacionNombre>(), isA<MostrandoNombreConfirmado>()],
  );

  blocTest<BlocVerificacion, EstadoVerificacion>(
    'cuando nombreRecibido es amlo debo tener MostrandoNombreConfirmado',
    build: () => BlocVerificacion(),
    seed: () => SolicitandoNombre(),
    act: (bloc) => bloc.add(NombreRecibido(NickFormado.constructor('amlo'))),
    expect: () => [
      isA<EsperandoConfirmacionNombre>(),
      isA<MostrandoNombreNoConfirmado>()
    ],
  );

  blocTest<BlocVerificacion, EstadoVerificacion>(
    'cuando nombreRecibido es xlmincorrecto debo tener MostrandoNombreConfirmado',
    build: () => BlocVerificacion(),
    seed: () => SolicitandoNombre(),
    act: (bloc) =>
        bloc.add(NombreRecibido(NickFormado.constructor('incorrecto'))),
    expect: () => [
      isA<EsperandoConfirmacionNombre>(),
      isA<MostrandoSolicitudActualizacion>()
    ],
  );
}
