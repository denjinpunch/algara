import 'dart:io';
import 'package:bloc/bloc.dart';
import 'package:tallerex1/caracteristicas/dominio/nick_formado.dart';
import 'package:tallerex1/caracteristicas/dominio/juego_jugado.dart';
class EventoVerificacion {}

class Creado extends EventoVerificacion {}

class NombreRecibido extends EventoVerificacion {
  final NickFormado nick;
  NombreRecibido(this.nick);
}

class NombreConfirmado extends EventoVerificacion {}

class EstadoVerificacion {}

class Creandose extends EstadoVerificacion {}

class SolicitandoNombre extends EstadoVerificacion {}

class MostrandoNombre extends EstadoVerificacion {}

class MostrandoNombreConfirmado extends EstadoVerificacion {
 final Set<JuegoJugado> juegoJugado;
  MostrandoNombreConfirmado(this.juegoJugado);
}
class MostrandoJuegosJugados extends EstadoVerificacion{
  //arigato calvo-sama
  List<List<String>> juegoJugado;
  final String name;
  MostrandoJuegosJugados(this.juegoJugado,this.name);
}
class MostrarJuegosJugados extends EventoVerificacion{
  final String name;
  MostrarJuegosJugados(this.name);
}
class EsperandoConfirmacionNombre extends EstadoVerificacion {}

class MostrandoSolicitudActualizacion extends EstadoVerificacion {}

class MostrandoNombreNoConfirmado extends EstadoVerificacion {
  final NickFormado nick;
  MostrandoNombreNoConfirmado(this.nick);
}

class BlocVerificacion extends Bloc<EventoVerificacion, EstadoVerificacion> {
  BlocVerificacion() : super(Creandose()) {
    on<Creado>((event, emit) {
      emit(SolicitandoNombre());
    });
    on<NombreRecibido>((event, emit){
       String direction="";
        if (event.nick.valor =='benthor') {
        direction= File('./test/caracteristicas/datos_txt/benthor.txt').readAsStringSync();
      }
      if (event.nick.valor=='fokuleh') {
        direction= File('./test/caracteristicas/datos_txt/fokuleh.txt').readAsStringSync();
      }
      List<List<String>> game = [];
      for (var juego in direction.split('\n')) {
        if (juego != "") {
          String id = juego.split(' - ')[0];
          String name = juego.split(' - ')[1];
          String icon = juego.split(' - ')[2];
          String designer = juego.split(' - ')[3];
          List<String>data=[id,name,icon,designer];
          game.add(data);

        }
      }
      emit(MostrandoJuegosJugados(game, event.nick.valor));
    });
  }
}
