import 'package:tallerex1/caracteristicas/dominio/nick_formado.dart';
import 'package:tallerex1/caracteristicas/dominio/registro_usuario.dart';
import 'package:xml/xml.dart';
import 'package:fpdart/fpdart.dart';
import 'package:tallerex1/caracteristicas/dominio/problemas.dart';
import 'package:http/http.dart' as http;

abstract class RepositorioVerificacion {
  Future<Either<Problema, RegistroUsuario>> obtenerRegistroUsuario(
      NickFormado nick);
}

class RepositorioReal extends RepositorioPruebasVerificacion {
  @override
  Future<Either<Problema, RegistroUsuario>> obtenerRegistroUsuario(
      NickFormado nick) async {
    final resultado = await _obtenerXmlReal(nick.valor);
    return resultado.match((l) => left(l), (r) {
      XmlDocument documento = XmlDocument.parse(r);
      final registro = obtenerRegistroUsuarioDesdeXML(documento);
      return registro.match((l) => Left(l), (r) => Right(r));
    });
  }

  Future<Either<Problema, String>> _obtenerXmlReal(String valor) async {
    Uri direccion =
        Uri.https('www.boardgamegeek.com', 'xmlapi2/user', {'name': valor});
    Uri direccion2 = 
    Uri.https('www.boardgamegeek.com', 'xmlapi2/plays', {'name': valor});
    final respuesta = await http.get(direccion);
    if (respuesta.statusCode != 200) {
      return Left(ServidorNoAlcanzado());
    }
    return right(respuesta.body);
  }
}

class RepositorioPruebasVerificacion extends RepositorioVerificacion {
  final String _benthor = """
      <?xml version="1.0" encoding="utf-8"?>
      <user id="597373" name="benthor" termsofuse="https://boardgamegeek.com/xmlapi/termsofuse">
        <firstname value="Benthor" />			
        <lastname value="Benthor" />			
        <avatarlink value="N/A" />			
        <yearregistered value="2012" />		
      	<lastlogin value="2022-05-31" />		
        <stateorprovince value="" />			
        <country value="" />			
        <webaddress value="" />			
        <xboxaccount value="" />			
        <wiiaccount value="" />			
        <psnaccount value="" />			
        <battlenetaccount value="" />			
        <steamaccount value="" />			
        <traderating value="0" />	
				</user>

    """;
  final _amlo = """
        <?xml version="1.0" encoding="utf-8"?>
        <user id="" name="" termsofuse="https://boardgamegeek.com/xmlapi/termsofuse">
          <firstname value="" />			
          <lastname value="" />			
          <avatarlink value="N/A" />			
          <yearregistered value="" />			
          <lastlogin value="" />			
          <stateorprovince value="" />			
          <country value="" />			
          <webaddress value="" />			
          <xboxaccount value="" />			
          <wiiaccount value="" />			
          <psnaccount value="" />			
          <battlenetaccount value="" />			
          <steamaccount value="" />			
          <traderating value="362" />	
				</user>
  """;
  final String _incorrecto = """
        <?xml version="1.0" encoding="utf-8"?>
        <user id="" name="" termsofuse="https://boardgamegeek.com/xmlapi/termsofuse">
          <firstname value="" />			
          <lastname value="" />			
          <avatarlink value="N/A" />			
          <yearegistered value="" />			
          <lastlogin value="" />			
          <stateorprovince value="" />			
          <country value="" />			
          <webaddress value="" />			
          <xboxaccount value="" />			
          <wiiaccount value="" />			
          <psnaccount value="" />			
          <battlenetaccount value="" />			
          <steamaccount value="" />			
          <traderating value="362" />	
				</user>
  """;

  @override
  Future<Either<Problema, RegistroUsuario>> obtenerRegistroUsuario(
      NickFormado nick) {
    if (nick.valor == 'benthor') {
      final documento = XmlDocument.parse(_benthor);
      return Future.value(obtenerRegistroUsuarioDesdeXML(documento));
    }
    if (nick.valor == 'amlo') {
      final documento = XmlDocument.parse(_amlo);
      return Future.value(obtenerRegistroUsuarioDesdeXML(documento));
    }
    if (nick.valor == 'incorrecto') {
      final documento = XmlDocument.parse(_incorrecto);
      return Future.value(obtenerRegistroUsuarioDesdeXML(documento));
    }
    return Future.value(Left(UsuarioNoRegistrado()));
  }
}

Either<Problema, RegistroUsuario> obtenerRegistroUsuarioDesdeXML(
    XmlDocument documento) {
  const campoAnio = 'yearregistered';
  const campoNombre = 'firstname';
  const campoEstado = 'stateorprovince';
  const campoPais = 'country';
  const campoApellidos = 'lastname';

  Either<Problema, String> anioRegistrado =
      obtenerValorCampo(documento, campoAnio);
  Either<Problema, String> nombre = obtenerValorCampo(documento, campoNombre);
  Either<Problema, String> pais = obtenerValorCampo(documento, campoPais);
  Either<Problema, String> estado = obtenerValorCampo(documento, campoEstado);
  Either<Problema, String> apellidos =
      obtenerValorCampo(documento, campoApellidos);

  if ([anioRegistrado, nombre, pais, estado, apellidos]
      .any((element) => element.isLeft())) {
    return left(VersionIncorrectaXml());
  }

  final valorRegistrado = [anioRegistrado, apellidos, estado, nombre, pais]
      .map((e) => e.getOrElse((l) => ''))
      .toList();
  if (valorRegistrado[0].isEmpty) {
    return left(UsuarioNoRegistrado());
  }

  return right(RegistroUsuario.constructor(
      propuestaAnio: valorRegistrado[0],
      propuestaApellido: valorRegistrado[1],
      propuestaEstado: valorRegistrado[2],
      propuestaNombre: valorRegistrado[3],
      propuestaPais: valorRegistrado[4])); //RegistroUsuario.constructor //
}

Either<Problema, String> obtenerValorCampo(
    XmlDocument documento, String campo) {
  const atributoValor = 'value';
  final valoresEncontrados = documento.findAllElements(campo);
  if (valoresEncontrados.isEmpty) return left(VersionIncorrectaXml());

  final String? valorARegresar =
      valoresEncontrados.first.getAttribute(atributoValor);
  if (valorARegresar == null) {
    return left(VersionIncorrectaXml());
  }
  return Right(valorARegresar);
}
