import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tallerex1/caracteristicas/bloc.dart';

class VistaMostrarJuegos extends StatelessWidget {
  final List<List<String>> juegoJugado;
  final String name;
  const VistaMostrarJuegos(this.juegoJugado,this.name, {super.key});

  @override
  Widget build(BuildContext context) {
    final elBloc = context.read<BlocVerificacion>();
    return MaterialApp(
      title: name,
      theme:  ThemeData(
        primarySwatch: Colors.pink,
      ),
      home: Scaffold(
          appBar: AppBar(leading: IconButton(
                    icon: Icon(Icons.arrow_back_ios),
                    iconSize: 20.0,
                    onPressed: () {
                      elBloc.add(Creado());
                    },
                  ),
              title: Text('Juegos jugados de $name')
          ),
          body:  ListView(
              children: juegoJugado.map(_buildItem).toList()
           ) 
      )
    );
  }
}
Widget _buildItem(List<String> data) {
  return  ListTile(
      title:  Text("${data[0]} - ${data[1]}"),
      subtitle: Text(data[3]),
      leading:  CachedNetworkImage(
            placeholder: (context, url) => const CircularProgressIndicator(),
            imageUrl: data[2],
          ),
      onTap: (){
      },
  );
}