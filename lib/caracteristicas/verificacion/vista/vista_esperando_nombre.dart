import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tallerex1/caracteristicas/bloc.dart';
import 'package:tallerex1/caracteristicas/dominio/nick_formado.dart';

class VistaSolicitandoNombre extends StatefulWidget {
  const VistaSolicitandoNombre({Key? key}) : super(key: key);

  @override
  State<VistaSolicitandoNombre> createState() => _VistaSolicitandoNombreState();
}

class _VistaSolicitandoNombreState extends State<VistaSolicitandoNombre> {
  bool _validation = false;
  final _controlador = TextEditingController();
 @override
  void initState() {
    _controlador.addListener(listener);
    super.initState();
  }
   void listener() {
    setState(() {
      try {
        NickFormado.constructor(_controlador.text);
        _validation = true;
      } catch (e) {
        _validation = false;
      }
    });
  }
  @override
  Widget build(BuildContext context) {
    final elBloc = context.read<BlocVerificacion>();
    return Column(
      children: [
        const Text('Dame el nombre'),
        TextField(
          controller: _controlador,
        ),
        Container(
            child: _validation
                ? null
                : TextButton(
                    style: ButtonStyle(
                      foregroundColor:
                          MaterialStateProperty.all<Color>(Colors.white),
                    ),
                    onPressed: null,
                    child: const Text('Ingresar'),
                  )),
        Container(
            child: !_validation
                ? null
                : TextButton(
                    style: ButtonStyle(
                      foregroundColor:
                          MaterialStateProperty.all<Color>(Colors.blue),
                    ),
                    onPressed: () {
                      elBloc.add(NombreRecibido(NickFormado.constructor(_controlador.text)));
                    },
                    child: const Text('Ingresar'),
                  ))
    ],
    );
  }
  @override
  void dispose() {
    _controlador.dispose();
    super.dispose();
  }
}
