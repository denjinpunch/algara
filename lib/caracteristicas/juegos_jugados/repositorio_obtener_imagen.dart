import 'dart:io';
import 'package:fpdart/fpdart.dart';
import 'package:tallerex1/caracteristicas/dominio/nick_formado.dart';
import 'package:tallerex1/caracteristicas/dominio/problemas.dart';
import 'package:xml/xml.dart';
import 'package:http/http.dart' as http;

abstract class ImagenIDXml {
  Future<Either<Problema, List<String>>> obtenerThmb(String id);
}

class OnlineXmlImg extends ImagenIDXml {
  @override
  Future<Either<Problema, List<String>>> obtenerThmb(String id) async {
    final respuesta = await http
        .get(Uri.parse('https://boardgamegeek.com/xmlapi2/thing?id=$id'));
    if (respuesta.statusCode != 200) {
      return Left(ServidorNoAlcanzado());
    }
    var resultado = _onlineThmb(respuesta.body);
    return resultado.match((l) {
        return left(l);
      }, (r) {
        return right(r);
      });

  }
}
class OfflineXmlImg extends ImagenIDXml {
  @override
  Future<Either<Problema, List<String>>> obtenerThmb(String id) async {
  try{
    String respuesta = File("./test/caracteristicas/dominio/xml/id.xml").readAsStringSync();
    var resultado = _onlineThmb(respuesta);
    return resultado.match((l) {
        return left(l);
      }, (r) {
        return right(r);
      });
  }catch(e){
    return Left(VersionIncorrectaXml());
      } 
   }
}
Either<Problema, List<String>> _onlineThmb(String elXML) {
     List<String> data=[];
    try {
      XmlDocument document = XmlDocument.parse(elXML);
      final thmb = document.findAllElements('thumbnail').first.children;
      final link = document.findAllElements('link');
      String designer = "";
      for (var i in link) {
        if (i.getAttribute('type') =='boardgamedesigner') {
        designer = i.getAttribute('value').toString();
        }
      }
      data.add(thmb.first.toString());
      data.add(designer);
      return Right(data);
    } catch (e) {
      return Left(VersionIncorrectaXml());
    }
  }


