import 'dart:io';
import 'package:fpdart/fpdart.dart';
import 'package:tallerex1/caracteristicas/dominio/nick_formado.dart';
import 'package:tallerex1/caracteristicas/dominio/problemas.dart';
import 'package:xml/xml.dart';
import 'package:http/http.dart' as http;

abstract class RepositorioXml {
  Future<Either<Problema, List<String>>> obtenerXml(NickFormado nick);
}

abstract class ImagenIDXml {
  Future<Either<Problema, String>> obtenerThmb(String id);
}

class OnlineXml extends RepositorioXml {
  @override
  Future<Either<Problema, List<String>>> obtenerXml(NickFormado nick) async {
    try {
      final respuesta = await http.get(Uri.parse(
          'https://boardgamegeek.com/xmlapi2/plays?username=${nick.valor}'));
      String elXml = respuesta.body;
      int cuantasPaginas = _obtenerCuantasPaginasDesdeOnline(elXml);
      List<String> nombresPaginas =
          _obtenerJuegosJugadosDesdeOnline(cuantasPaginas, nick);
      List<String> resultado = [];
      for (var pagina in nombresPaginas) {
        final request = await http.get(Uri.parse(pagina));
        resultado.add(request.body);
      }
      return Right(resultado);
    } catch (e) {
      return Left(VersionIncorrectaXml());
    }
  }

  List<String> _obtenerJuegosJugadosDesdeOnline(
      cuantasPaginas, NickFormado nick) {
    var base = 'https://boardgamegeek.com/xmlapi2/plays?username=${nick.valor}';
    List<String> lista = [];
    for (var i = 1; i <= cuantasPaginas; i++) {
      lista.add('$base&page=$i');
    }
    return lista;
  }

  int _obtenerCuantasPaginasDesdeOnline(String xml) {
    XmlDocument documento = XmlDocument.parse(xml);
    String cadenaNumero = documento.getElement('plays')!.getAttribute('total')!;
    int numero = int.parse(cadenaNumero);
    return (numero / 100).ceil();
  }
}

class RepositorioXmlPruebas extends RepositorioXml {
  final tamanoPagina = 2;
  @override
  Future<Either<Problema, List<String>>> obtenerXml(NickFormado nick) async {
    if (!['Benthor', 'Fokuleh'].contains(nick.valor))
      return left(UsuarioNoRegistrado());

    try {
      String elXml = File(
              './test/caracteristicas/dominio/xml/${nick.valor.toLowerCase()}1.xml')
          .readAsStringSync();
      int cuantosPaginas = _obtenerCuantasPaginasDesdeXml(elXml);
      List<String> nombresPaginas =
          _obtenerNombresPaginas(cuantosPaginas, nick);
      return Right(
          nombresPaginas.map((e) => File(e).readAsStringSync()).toList());
    } catch (e) {
      return left(VersionIncorrectaXml());
    }
  }

  int _obtenerCuantasPaginasDesdeXml(String elXml) {
    XmlDocument documento = XmlDocument.parse(elXml);
    String cadenaNumero = documento.getElement('plays')!.getAttribute('total')!;
    int numero = int.parse(cadenaNumero);
    return (numero / tamanoPagina).ceil();
  }

  List<String> _obtenerNombresPaginas(int cuantasPaginas, NickFormado nick) {
    final base = './test/caracteristicas/dominio/xml/${nick.valor}';
    List<String> lista = [];
    for (var i = 1; i <= cuantasPaginas; i++) {
      lista.add('$base$i.Xml');
    }
    return lista;
  }
}
