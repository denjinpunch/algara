import 'package:fpdart/fpdart.dart';
import 'package:tallerex1/caracteristicas/dominio/juego_jugado.dart';
import 'package:tallerex1/caracteristicas/dominio/nick_formado.dart';
import 'package:tallerex1/caracteristicas/dominio/problemas.dart';
import 'package:tallerex1/caracteristicas/juegos_jugados/repositorio_xml.dart';
import 'package:xml/xml.dart';

abstract class RepositorioJuegosJugados {
  final RepositorioXml repositorioXml;
  RepositorioJuegosJugados(this.repositorioXml);
  Future<Either<Problema, Set<JuegoJugado>>> obtenerJuegosJugados(
      NickFormado nick);
}

class RepositorioJuegosJugadosPruebas extends RepositorioJuegosJugados {
  RepositorioJuegosJugadosPruebas(repositorio) : super(repositorio);
  @override
  Future<Either<Problema, Set<JuegoJugado>>> obtenerJuegosJugados(
      NickFormado nick) async {
    final Either<Problema, List<String>> resultadoXml =
        await super.repositorioXml.obtenerXml(nick);
    return resultadoXml.match((l) {
      return left(l);
    }, (r) {
      final resultado = _obtenerJuegosJugadosDesdeXml(r);
      return resultado;
    });
  }

  Either<Problema, Set<JuegoJugado>> _obtenerJuegosJugadosDesdeXml(
      List<String> elXml) {
    final resultado = elXml.map((e) => _obtenerUnSoloSet(e));
    if (resultado.any((element) => element is Problema)) {
      return Left(VersionIncorrectaXml());
    }
    final soloSets = resultado.map((e) => e.getOrElse((l) => {}));
    Set<JuegoJugado> conjunto = {};
    soloSets.forEach((element) {
      conjunto.addAll(element.toList());
    });
    soloSets.fold<Set<JuegoJugado>>({}, (p, a) => a..addAll(p.toList()));
    return right(conjunto);
  }

  Either<Problema, Set<JuegoJugado>> _obtenerUnSoloSet(String elXml) {
    try {
      XmlDocument documento = XmlDocument.parse(elXml);
      final losPlay = documento.findAllElements('item');
      final conjuntoIterable = losPlay.map((e) {
        String nombre = e.getAttribute('name')!;
        String id = e.getAttribute('objectid')!;
        return JuegoJugado.constructor(
            nombrePropuesto: nombre, idPropuesto: id);
      });
      final conjunto = Set<JuegoJugado>.from(conjuntoIterable);
      return Right(conjunto);
    } catch (e) {
      return left(VersionIncorrectaXml());
    }
  }
}

Either<Problema, int> numeroDePaginas(String elXml) {
  return numeroDePaginas(elXml);
}
